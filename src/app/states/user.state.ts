import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { GetUsers } from '../actions/user.action';

export class UserStateModel {
  users: User[];
 
}

@State<UserStateModel>({
  name: 'users',
  defaults: {
    users: []
  }
})
@Injectable()
export class UserState {

  constructor(private userService: UserService) {
  }

  @Selector()
  static getUserList(state: UserStateModel) {
    return state.users;
  }


  @Action(GetUsers)
  getUsers({ getState, setState }: StateContext<UserStateModel>) {
    return this.userService.users().pipe(tap((result: User[]) => {
      const state = getState();
      setState({
        ...state,
        users: result,
      });
    }));
  }

}
