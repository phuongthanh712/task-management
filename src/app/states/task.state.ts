import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { AddTask, FilterTasks, GetTasks, SetSelectedTaskId, UpdateTask } from '../actions/task.action';
import { shareReplay, tap } from 'rxjs/operators';
import { TaskService } from '../services/task.service';
import { Task } from '../models/task.model';
import { Injectable } from '@angular/core';
import { UserState } from './user.state';
export class TaskStateModel {
  tasks: Task[];
  selectedTask: Task;
  filter: {
    description?: string;
    completed?: boolean;
  }
}

@State<TaskStateModel>({
  name: 'tasks',
  defaults: {
    tasks: [],
    selectedTask: null,
    filter: undefined
  }
})
@Injectable()
export class TaskState {

  constructor(private taskService: TaskService, private store: Store) {
  }

  @Selector()
  static getTaskList(state: TaskStateModel) {
    return state.tasks;
  }

  @Selector()
  static getFilteredTask(state: TaskStateModel) {
    if (state.filter && (state.filter.completed !== undefined || state.filter.description)) {
      const completeCondition = (x) => state.filter.completed === undefined || x.completed === state.filter.completed;
      const descriptionCondition = (x) => !state.filter.description || x.description.toLowerCase().includes(state.filter.description.toLowerCase());
      return state.tasks.filter(x => completeCondition(x) && descriptionCondition(x));
    }
    return state.tasks;
  }

  @Action(FilterTasks)
  updateFilter({ patchState }: StateContext<TaskStateModel>, { payload }: FilterTasks) {
    patchState({ filter: payload });
  }

  @Selector()
  static getSelectedTask(state: TaskStateModel) {
    return state.selectedTask;
  }

  private addAssigneeDisplayName(task: Task) {
    const users = this.store.selectSnapshot(UserState.getUserList);
    return {
      ...task,
      assigneeDisplayName: isFinite(task.assigneeId) ? users.find(x => x.id === task.assigneeId)?.name : ''
    };
  }

  @Action(GetTasks)
  getTasks({ getState, setState }: StateContext<TaskStateModel>) {
    return this.taskService.tasks().pipe(tap((result: Task[]) => {
      const addedDisplayNameTasks = result && result.map(task => { return this.addAssigneeDisplayName(task) });
      const state = getState();
      setState({
        ...state,
        tasks: addedDisplayNameTasks // result,
      });
    }));
  }

  @Action(AddTask)
  addTask({ getState, patchState }: StateContext<TaskStateModel>, { payload }: AddTask) {
    return this.taskService.newTask(payload).pipe(
      tap((result) => {
        const state = getState();
        patchState({
          tasks: [...state.tasks, this.addAssigneeDisplayName(result)],
          selectedTask: result
        });
      }),
    );
  }

  @Action(UpdateTask)
  updateTask({ getState, setState }: StateContext<TaskStateModel>, { payload, id }: UpdateTask) {
    return this.taskService.update(id, payload).pipe(
      tap((result) => {
        const state = getState();
        const taskList = [...state.tasks];
        const taskIndex = taskList.findIndex(item => item.id === id);
        taskList[taskIndex] = this.addAssigneeDisplayName(result);
        setState({
          ...state,
          tasks: taskList,
          selectedTask: taskList[taskIndex]
        });
      })
    );
  }

  @Action(SetSelectedTaskId)
  setSelectedTaskId({ getState, setState }: StateContext<TaskStateModel>, { id }: SetSelectedTaskId) {
    const state = getState();
    const selectedTask = state?.tasks.find(x => x.id === id);
    if (!selectedTask) {
      return this.taskService.task(id).pipe(tap((result) => {
        if (result) {
          setState({
            ...state,
            selectedTask: this.addAssigneeDisplayName(result)
          });
        }
      }));
    } else {
      setState({
        ...state,
        selectedTask: this.addAssigneeDisplayName(selectedTask)
      });
    }
  }
}
