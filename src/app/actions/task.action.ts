import { Task } from '../models/task.model';

export class AddTask {
  static readonly type = '[Task] Add';

  constructor(public payload: Task) {
  }
}

export class GetTasks {
  static readonly type = '[Task] Get';
}

export class UpdateTask {
  static readonly type = '[Task] Update';

  constructor(public payload: Task, public id: number) {
  }
}

export class CompleteTask {
  static readonly type = '[Task] Complete';

  constructor(public id: number, public completed: boolean) {
  }
}

export class FilterTasks {
  static readonly type = '[Filter] Task';
  constructor(public payload: Partial<Task>) { }
}

export class SetSelectedTaskId {
  static readonly type = '[Task] Set';
  constructor(public id: number) {
  }
}
