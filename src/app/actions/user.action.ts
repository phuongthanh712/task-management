import { User } from '../models/user.model';


export class GetUsers {
  static readonly type = '[User] Get';
}
