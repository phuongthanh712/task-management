import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetTasks } from './actions/task.action';
import { GetUsers } from './actions/user.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isCollapsed = false;
  constructor(private store: Store) {
  }

  ngOnInit() {
    this.store.dispatch([new GetUsers(), new GetTasks()]);
  }
}
