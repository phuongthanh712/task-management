import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskListComponent } from './task-list/task-list.component';

const routes: Routes = [
  { path: '', component: TaskListComponent },
  { path: 'create', component: TaskDetailComponent },
  { path: 'edit/:id', component: TaskDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    NzTableModule],
  exports: [RouterModule]
})
export class TaskRoutingModule { }
