import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { Task } from 'src/app/models/task.model';
import { UserState } from 'src/app/states/user.state';
import { TaskState } from 'src/app/states/task.state';
import { AddTask, SetSelectedTaskId, UpdateTask } from 'src/app/actions/task.action';
import { GetUsers } from 'src/app/actions/user.action';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss']
})
export class TaskDetailComponent implements OnInit {
  editMode = false;
  form: FormGroup;

  defaultTask: Task = {
    id: -1,
    description: '',
    assigneeId: null,
    completed: false
  }
  selectedTask: Task;

  @Select(UserState.getUserList) users: Observable<User[]>;

  @Select(TaskState.getSelectedTask) task: Observable<Task>;

  constructor(private fb: FormBuilder, private store: Store,
    private route: ActivatedRoute, private router: Router,
    private modal: NzModalService) {
    this.createForm();
  }

  ngOnInit(): void {
    this.route.params.subscribe((param) => {
      const id = param['id'];
      this.editMode = isFinite(id);
      if (this.editMode) {
        this.editMode = true;
        this.store.dispatch(new SetSelectedTaskId(+id));
        this.task.subscribe(result => {
          if (result) {
            this.selectedTask = result;
            this.form.patchValue({
              description: result.description,
              assigneeId: result.assigneeId,
              completed: result.completed
            });
          }
        });
      }
    })
  }

  private createForm() {
    this.form = this.fb.group({
      description: [this.defaultTask.description, [Validators.required]],
      assigneeId: [this.defaultTask.assigneeId, [Validators.required]],
      completed: [this.defaultTask.completed],
    });
  }

  submitForm(values: any) {
    if (this.editMode) {
      this.store.dispatch(new UpdateTask(this.form.value, this.selectedTask.id)).subscribe(() => {
        this.modal.success({ nzContent: 'Update successfully!' });
      })
    } else {
      this.store.dispatch(new AddTask(this.form.value)).subscribe((res) => {
        this.modal.success({ nzContent: 'Create new task successfully!' });
      })
    }
  }

  back() {
    this.router.navigate(['task']);
  }
}
