import { Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FilterTasks, GetTasks, SetSelectedTaskId, UpdateTask } from 'src/app/actions/task.action';
import { TaskState } from 'src/app/states/task.state';
import { Task } from 'src/app/models/task.model';
import { Router } from '@angular/router';
import { GetUsers } from 'src/app/actions/user.action';
import { UserState } from 'src/app/states/user.state';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnDestroy {
  searchValue = '';
  visible = false;
  filterComplete = [
    { text: 'Completed', value: 'completed' },
    { text: 'In progress', value: 'incomplete' }
  ];

  filterAssignee = [];

  filter = {};

  @Select(TaskState.getFilteredTask) tasks: Observable<Task[]>;

  constructor(private store: Store, private router: Router) {
  }

  ngOnDestroy() {
    this.resetFilters();
  }

  edit(data: Task) {
    this.store.dispatch(new SetSelectedTaskId(data.id));
    this.router.navigate([`/task/edit/${data.id}`]);
  }

  add() {
    this.router.navigate(['/task/create']);
  }

  markAsComplete(data: Task) {
    this.store.dispatch(new UpdateTask({ ...data, completed: true }, data.id));
  }

  resetFilters() {
    this.filter = {};
    this.searchValue = '';
    this.updateFilter();
  }

  resetDescriptionFilter(): void {
    this.searchValue = '';
    this.search();
  }

  search(): void {
    this.visible = false;
    this.filter = { ...this.filter, description: this.searchValue };
    this.updateFilter();
  }

  completeFilter(values: string[]) {
    if (!values || values && (!values.length || values.length === 2)) {
      // select all mean no filter
      this.filter = { ...this.filter, completed: undefined };
    }
    if (values && values.length === 1) {
      this.filter = { ...this.filter, completed: values.includes('completed') };
    }
    this.updateFilter();
  }

  private updateFilter() {
    this.store.dispatch(new FilterTasks(this.filter));
  }
}
