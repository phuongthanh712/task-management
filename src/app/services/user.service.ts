import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { User } from '../models/user.model';
import { Task } from '../models/task.model';
 

function randomDelay() {
  return Math.random() * 1000;
}

@Injectable({ providedIn: 'root' })
export class UserService {
  storedUsers: User[] = [
    { id: 111, name: "Mike" },
    { id: 222, name: "James" }
  ];

  private findUserById = id => this.storedUsers.find(user => user.id === +id);

  users() {
    return of(this.storedUsers).pipe(delay(randomDelay()));
  }

  user(id: number) {
    return of(this.findUserById(id)).pipe(delay(randomDelay()));
  }
}
