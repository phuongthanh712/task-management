import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { Task } from '../models/task.model';

/**
 * This service acts as a mock backend.
 *
 * You are free to modify it as you see.
 */


function randomDelay() {
  return Math.random() * 1000;
}

@Injectable({ providedIn: 'root' })
export class TaskService {
  storedTasks: Task[] = [
    {
      id: 0,
      description: "Install a monitor arm",
      assigneeId: 111,
      completed: false
    },
    {
      id: 1,
      description: "Move the desk to the new location",
      assigneeId: 111,
      completed: false
    },
    {
      id: 2,
      description: "Implement task management site",
      assigneeId: 222,
      completed: true
    }
  ];

  lastId = 2;

  private findTaskById = id =>
    this.storedTasks.find(task => task.id === +id);


  tasks() {
    return of(this.storedTasks).pipe(delay(randomDelay()));
  }

  task(id: number): Observable<Task> {
    return of(this.findTaskById(id)).pipe(delay(randomDelay()));
  }


  newTask(payload: Partial<Task>) {
    const newTask: Task = {
      id: ++this.lastId,
      description: payload.description,
      assigneeId: payload.assigneeId ?? null,
      completed: payload.completed ?? false
    };

    this.storedTasks = this.storedTasks.concat(newTask);

    return of(newTask).pipe(delay(randomDelay()));
  }

  assign(taskId: number, userId: number) {
    return this.update(taskId, { assigneeId: userId });
  }

  complete(taskId: number, completed: boolean) {
    return this.update(taskId, { completed });
  }

  update(taskId: number, updates: Partial<Omit<Task, "id">>) {
    const foundTask = this.findTaskById(taskId);

    if (!foundTask) {
      return throwError(new Error("task not found"));
    }

    const updatedTask = { ...foundTask, ...updates };

    this.storedTasks = this.storedTasks.map(t =>
      t.id === taskId ? updatedTask : t
    );

    return of(updatedTask).pipe(delay(randomDelay()));
  }
}
